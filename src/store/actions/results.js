import * as actionTypes from './actions';

export const saveResult = (res) => {
	const updatedRes = res * 2;
	return {
		type: actionTypes.STORE_RESULT,
		result: updatedRes
	};
};

export const storeResult = (res) => {
	return (dispatch, getState) => {
		setTimeout(() => {
			const oldCounter = getState().counterReducer.counter;
			console.log(oldCounter);
			dispatch(saveResult(res));
		}, 2000)
	};
};

export const deleteResult = (id) => {
	return {
		type: actionTypes.DELETE_RESULT,
		resultId: id
	};
};
